# ![MesoizoicSolution](imgs/logo.png)

A solution for learning base of OOP and Csharp with .Net Core


## UML Diagramm

![UML diagramm](imgs/uml.png)

## How to ?

**Run unit tests ?**

```bash
cd mesozoictest
dotnet test
```

**Run console app ?**

```bash
cd mesozoicconsole
dotnet run
```

## Projects

MesozoicSolution is made of two projects

### MesozoicConsole

A dummy console application for manipulating some dinosaurs

### MesozoicTest

A dummy Xunit collection of unit tests for learning Unit test !

