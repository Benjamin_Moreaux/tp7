﻿using System;
using Mesozoic;

namespace mesozoicconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello from Mesozoic!");
            Dinosaur louis, yacine, michaile;

            louis = new Dinosaur("Louis", "Stegausaurus", 12);
            yacine = new Dinosaur("Yacine", "Diplodocus", 20);

            Console.WriteLine("Présentation de nos dinosaures:");

            Console.WriteLine(louis.SayHello());
            Console.WriteLine(yacine.Roar());

            Console.WriteLine(louis.Hug(nessie));
            Console.WriteLine(yacine.SayHello());

            Console.WriteLine(louis.Hug(louis));   
            Console.WriteLine(yacine.Hug(louis));

            Console.WriteLine("\nCréation d'un troupeau");

            Horde horde = new Horde();
            horde.AddDinosaur(louis);
            horde.AddDinosaur(yacine);

            Console.WriteLine(horde.IntroduceAll());
        }
    }
}
