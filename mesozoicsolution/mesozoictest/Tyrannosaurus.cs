using System;
using Xunit;

using Mesozoic;

namespace stegausaurustest
{
    public class DinosaurTest
    {
        [Fact]
        public void TestConstructor()
        {
            Dinosaur michaile = new Dinosaur("Michaile", "TyrannosaurusRex", 15);

            Assert.Equal("Michaile", michaile.GetName());
            Assert.Equal("TyrannosaurusRex", michaile.GetSpecie());
            Assert.Equal(15, michaile.GetAge());
        }

        [Fact]
        public void TestRoar()
        {
            Dinosaur michaile = new Dinosaur("Michaile", "TyrannosaurusRex", 15);
            Assert.Equal("Grrr", michaile.Roar());
        }

        [Fact]
        public void TestSayHello()
        {
            Dinosaur michaile = new Dinosaur("Michaile", "TyrannosaurusRex", 15);
            Assert.Equal("Je suis Michaile le TyrannosaurusRex, j'ai 15 ans.", michaile.SayHello());
        }

        public void TestHug()
        {
            Dinosaur michaile = new Dinosaur("Michaile", "TyrannosaurusRex", 15);

            Assert.Equal("Je suis Michaile et je fais un câlin à Louis.", louis.michaile(Louis));
        }
    }
}
