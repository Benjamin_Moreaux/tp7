using System;
using Xunit;

using Mesozoic;
using Ingen;

namespace mesozoictest
{
    public class LaboratoryTest
    {
        [Fact]
        public void TestCreateDinosaur()
        {
            Dinosaur dinosaur = Laboratory.CreateDinosaur("Nessie", "Diplodocus");
            Assert.NotNull(dinosaur);
            Assert.Equal("Nessie", dinosaur.GetName());
            Assert.Equal("Diplodocus", dinosaur.GetSpecie());
            Assert.Equal(0, dinosaur.GetAge());
        }
    }
}