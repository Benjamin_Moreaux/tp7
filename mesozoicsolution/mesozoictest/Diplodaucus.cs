using System;
using Xunit;

using Mesozoic;

namespace diplodocusTest
{
    public class diplodocusTest
    {
        
        public void TestConstructor()
        {
            Dinosaur yacine = new Dinosaur("Yacine", "Diplodocus", 20);

            Assert.Equal("Yacine", yacine.GetName());
            Assert.Equal("Diplodocus", yacine.GetSpecie());
            Assert.Equal(20, yacine.GetAge());
        }

        public void TestRoar()
        {
            Dinosaur yacine = new Dinosaur("Yacine", "Diplodocus", 20);

            Assert.Equal("Grrr", yacine.Roar());
        }

        public void TestSayHello()
        {
            Dinosaur yacine = new Dinosaur("Yacine", "Diplodocus", 20);
            Assert.Equal("Je suis Nessie le Diplodocus, j'ai 11 ans.", nessie.SayHello());
        }


        public void TestHug()
        {
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Assert.Equal("Je suis Nessie et je fais un c�lin � Louis.", nessie.Hug(louis));
            Assert.Equal("Je suis Nessie et je ne peux pas me faire de c�lin � moi-m�me :'(.", nessie.Hug(nessie));
        }
    }
}