using System;
using Xunit;

using Mesozoic;

namespace stegausaurustest
{
    public class DinosaurTest
    {
        [Fact]
        public void TestConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.GetName());
            Assert.Equal("Stegausaurus", louis.GetSpecie());
            Assert.Equal(12, louis.GetAge());
        }

        [Fact]
        public void TestRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.Roar());
        }

        [Fact]
        public void TestSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.SayHello());
        }

        public void TestHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Je suis Louis et je fais un câlin à Nessie.", louis.Hug(nessie));
        }
    }
}
